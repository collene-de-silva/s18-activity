//Real World Application of Objects
/*
	-Scenario:
		1. We would like to create a new game that would have several pokemon interact with each other 
		2. Every pokemon should have the same set of properties and functions 

*/

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log(`This pokemon tacked targetPokemon`)
	},
	faint: function() {
		console.log('Pokemon fainted')
	}
}

console.log(myPokemon);

//We will use object constructor 

let targetHealth = 0



function Pokemon(name, level) {
	//properties 
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;



	//methods
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	};

	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name} `)
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
		target.health = target.health - this.attack
		if (target.health <=5){
			target.faint()
		}
	}
	
}


//Creates a new instance of the "Pokemon" object
let pikachu = new Pokemon("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 8);


pikachu.tackle(squirtle) 
pikachu.tackle(squirtle) 
/*
if (squirtle.health <=5) {
	squirtle.faint()
} else if (pikachu.health <=5) {
	pikachu.faint()
} else {
	console.log("Battle continues")
}*/